README file for Commerce Custom Offline Payments

CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* How it works
* Troubleshooting
* Maintainers

INTRODUCTION
------------
Solution for offline payments for Drupal Commerce.
ToDo


REQUIREMENTS
------------
ToDo


INSTALLATION
------------
ToDo


CONFIGURATION
-------------
ToDo


HOW IT WORKS
------------
ToDo


TROUBLESHOOTING
---------------
ToDo


MAINTAINERS
-----------
Current maintainers:
* Tavi Toporjinschi (vasike) - https://www.drupal.org/u/vasike

ToDo

